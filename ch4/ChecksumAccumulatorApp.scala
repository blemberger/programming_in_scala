import ChecksumAccumulator.calculate

object ChicagoTeamsSummer extends App {

  for (team <- List("Bears", "Blackhawks", "Cubs", "White Sox", "Bulls"))
    println(team + ": " + calculate(team))
}
