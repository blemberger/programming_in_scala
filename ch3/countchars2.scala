import scala.io.Source

if (args.length > 0) {
  // List to hold the lines of text in the file
  val lines = Source.fromFile(args(0)).getLines().toList
  // Iterate thru lines to find the maximum length line
  val maxLengthLine = lines.reduceLeft(
                      (a, b) => if (a.length > b.length) a else b )
  val maxWidthOfLength = widthOfLength(maxLengthLine) 

  // Iterate thru the list, printing out the file contents with each line length right-justified and whatever
  for (line <- lines) {
    val numSpaces = maxWidthOfLength - widthOfLength(line)
    val padding = " " * numSpaces
    println(padding + line.length + " | " + line)
  }
}
else
  Console.err.println("Please enter filename.")

/* Function that finds the length of the numerical string representing the length
   of a line of text */
def widthOfLength(s: String) : Int = s.length.toString.length
