// The old, verbose way to create an array
val greetStrings = new Array[String](3)
greetStrings(0) = "Hello"
greetStrings(1) = ", "
greetStrings(2) = "world!\n"

for (i <- 0 to 2)
  print(greetStrings(i))

// Parenthesis operators on retrieval are really just implied calls to apply() method
println(greetStrings.apply(0))

// Parenthesis operators on set are really just implied calls to apply() method
greetStrings.update(0, "Lune")
for (i <- 0 to 2)
  print(greetStrings(i))

// Best way to do it, using the Array companion object (like a static method call)
val rivers = Array("Euphrates", "Tigris", "Rubicon")
println("Crossing the " + rivers(2))
