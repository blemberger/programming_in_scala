// Sets are immutable by default in scala
// var can be reassigned to new immutable Set
var jetSet = Set("Boeing", "Airbus") 
jetSet += "Lear"
println(jetSet.contains("Lear"))
println(jetSet.contains("Cessna"))

import scala.collection.mutable.Set
// val does not get reassigned to new mutable Set
val authorSet = Set("Hitchens", "Murray", "Cohen") 
authorSet += "Harris"
println(authorSet)

// Can also use factory to create a concrete HashSet (or other) if desired
import scala.collection.immutable.HashSet
val hashSet = HashSet("Tomatoes", "Chilies")
println(hashSet + "Carrots")
