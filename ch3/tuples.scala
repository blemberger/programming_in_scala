// tuples can contain elements of different types
val triple = (1, "two", 3.1415)
println(triple._1)
println(triple._2)
println(triple._3)
