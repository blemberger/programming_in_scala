// immutable Map is the default
var romanNumeral = Map(1 -> "I", 2 -> "II", 3 -> "III",
                        4 -> "IV", 5 -> "V")
    println(romanNumeral(4))
// Won't work, as "+=" is not in immutable Maps
    romanNumeral += (6 -> "VI")
/* -> operator is a method on first operand. Takes second operand as value and
      returns a pair (first, second)
*/
   romanNumeral += ((7, "VII"))

import scala.collection.mutable.Map
// Must annotate types, otherwise Map doesn't know how to handle "+="
val treasureMap = Map[Int, String]()
treasureMap += (1 -> "Go to island.")
treasureMap += (2 -> "Find big X on ground.")
treasureMap += (3 -> "Dig.")
println(treasureMap(2))
