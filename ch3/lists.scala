val oneTwo= List(1, 2)
val threeFour= List(3, 4)
// append operator
val oneTwoThreeFour = oneTwo ::: threeFour
println(oneTwo + " and " + threeFour + " were not mutated.")
println("Thus, " + oneTwoThreeFour + " is a new list.")
// Cons operator
val twoThree = List(2, 3)
val oneTwoThree = 1 :: twoThree
println(oneTwoThree)
println(oneTwoThree.mkString(" and "))
val evensOdds = oneTwoThree.map(s => s % 2 == 0)
println(evensOdds)
println("odds only: " + oneTwoThreeFour.filter(s => s % 2 != 0))
