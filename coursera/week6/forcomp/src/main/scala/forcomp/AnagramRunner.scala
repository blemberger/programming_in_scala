package forcomp

import forcomp.Anagrams.{Sentence, sentenceAnagrams}

/* run this using the bash command:
    >sbt "run Hi Mary Jo"
 */
object AnagramRunner extends App {
  def printAnagrams(sen: Sentence) =
    for {
      s <- sentenceAnagrams(sen)
    } println(s)

  printAnagrams(args.toList)
}
