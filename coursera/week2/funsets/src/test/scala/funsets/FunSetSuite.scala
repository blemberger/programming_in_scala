package funsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  // test("string take") {
  //   val message = "hello, world"
  //   assert(message.take(5) == "hello")
  // }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
   test("adding ints") {
     assert(1 + 2 === 3)
   }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s = union(union(s1, s2), s3) // {1, 2, 3}
    val naturals: Set = isPositive // natural numbers

    def isPositive = (x: Int) => x > 0
    def isEven = (x: Int) => x % 2 == 0
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val u = union(s1, s2)
      assert(contains(u, 1), "Union 1")
      assert(contains(u, 2), "Union 2")
      assert(!contains(u, 3), "Union 3")
      assert(contains(union(u, s3), 3), "Union {1, 2, 3}")
      assert(contains(union(s1, s1), 1), "Union of set with itself")
    }
  }

  test("intersection contains only elements common to each set") {
    new TestSets {
      val u = union(s1, s2)
      val i = intersect(u, s1)
      assert(contains(i, 1), "Intersection of {1} and {1, 2} contains 1")
      assert(!contains(i, 2), "Intersection of {1} and {1, 2} does not contain 2")
      assert(contains(intersect(u, union(s2, s3)), 2), "Intersection of {1, 2} and {2, 3} contains 2")
      assert(!contains(intersect(u, union(s2, s3)), 3), "Intersection of {1, 2} and {2, 3} does not contain 3")
      assert(!contains(intersect(i, s3), 3), "Intersection of {1, 2} and {3} does not contain 3")
      assert(!contains(intersect(i, s3), 1), "Intersection of {1, 2} and {3} does not contain 1")
      assert(contains(intersect(s1, s1), 1), "Intersection of set with itself")
    }
  }

  test("diff contains only elements from the first set and not in the second set") {
    new TestSets {
      val u = union(s1, s2)
      val d = diff(u, s1) // {2}
      assert(contains(d, 2), "Diff of  {1, 2} and {1} contains 2")
      assert(contains(union(d, s1), 1), "union of diff contains original elements")
      assert(contains(union(d, s1), 2), "union of diff contains original elements")
      assert(contains(intersect(s, s2), 2), "Intersection of diff of {1, 2} and {1} with {1} contains 2")
      assert(!contains(diff(s1, s1), 1), "diff of set with itself does not contain is elements")
    }
  }

  test("filter set by predicate") {
    new TestSets {
      assert(contains(filter(s, (x: Int) => true), 2), "filtering with a tautology")
      assert(contains(filter(s, (x: Int) => x % 2 == 0), 2), "filtering even values includes 2")
      assert(!contains(filter(s, (x: Int) => x % 2 == 0), 1), "filtering even values does not include 1")
      assert(!contains(filter(emptySet(), (x: Int) => x % 2 == 0), 2), "filtering the empty set does not contain 1")
    }
  }

  test("forall returns true if predicate is true for every element") {
    new TestSets {
      assert(forall(s, isPositive), "for all {1, 2, 3} each element is positive")
      assert(!forall(s, isEven), "for all {1, 2, 3} each element is not even")
      assert(forall(naturals, isPositive), "all natural numbers are positive")
      assert(!forall(naturals, isEven), "all natural numbers are not even")
      assert(forall(emptySet(), isPositive), "for all elements in empty set is true")
    }
  }

  test("exists returns true if predicate is true for at least one element") {
    new TestSets {
      assert(exists(s, isPositive), "in {1, 2, 3} at least one element is positive")
      assert(exists(s, isEven), "in {1, 2, 3} at least one element is even")
      assert(exists(naturals, isPositive), "natural numbers have a positive member")
      assert(!exists(naturals, (x: Int) => x < 0), "natural numbers do not have a negative member")
      assert(exists(naturals, isEven), "natural numbers have an even member")
      assert(!exists(emptySet(), isPositive), "the empty set does not contain a positive member")
    }
  }

  test("map returns a new set by applying a function to each element of a set") {
    new TestSets {
      def multiplyByAHundred = (x: Int) => x * 100
      assert(contains(map(s, multiplyByAHundred), 100), "100 is in {100, 200, 300}")
      assert(contains(map(s, multiplyByAHundred), 200), "200 is in {100, 200, 300}")
      assert(contains(map(s, multiplyByAHundred), 300), "300 is in {100, 200, 300}")
      assert(!contains(map(s, multiplyByAHundred), 0), "0 is not in {100, 200, 300}")
      assert(!contains(map(s, multiplyByAHundred), 1), "1 is not in {100, 200, 300}")
      assert(!contains(map(s, multiplyByAHundred), 400), "400 is not in {100, 200, 300}")
      assert(!contains(map(emptySet(), multiplyByAHundred), 100), "100 is not in {}")
    }
  }
}
