package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int =
      if (c == 0 || c == r) 1 else pascal(c - 1, r -1) + pascal(c, r  - 1)
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def calculateNesting(c: Char, prevNesting: Int): Int =
        if (c == '(')
          prevNesting + 1
        else if (c == ')')
          prevNesting - 1
        else
          prevNesting

      def balanceRec(chars: List[Char], nestings: Int): Boolean = {
        if (chars.isEmpty)
          nestings == 0
        else {
          val newNestings = calculateNesting(chars.head, nestings)
          if (newNestings < 0)
            false
          else
            balanceRec(chars.tail, newNestings)
        }
      }
      balanceRec(chars, 0)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {

      def loopDenomination(money: Int, countDenomination: Int, accumWays: Int, coins: List[Int]): Int = {
        val denominationShare = coins.head * countDenomination
        if (denominationShare > money) accumWays // no way to make change with this denomination
        else if (denominationShare == money) accumWays + 1 // this is a way to make change
        else {
          // find all ways to make change with what's left after removing this denomination
          val ways = countChange(money - denominationShare, coins.tail)
          loopDenomination(money, countDenomination + 1, ways + accumWays, coins)
        }
      }

      if (coins.isEmpty || (money == 0)) 0 else loopDenomination(money, 0, 0, coins)
    }
  }
