package recfun

import org.scalatest.{FunSuite, Ignore}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountChangeSuite extends FunSuite {
  import Main.countChange
  test("countChange: example given in instructions") {
    assert(countChange(4,List(1,2)) === 3)
  }

  test("countChange: sorted CHF") {
    assert(countChange(300,List(5,10,20,50,100,200,500)) === 1022)
  }

  test("countChange: no pennies") {
    assert(countChange(301,List(5,10,20,50,100,200,500)) === 0)
  }

  test("countChange: unsorted CHF") {
    assert(countChange(300,List(500,5,50,100,20,200,10)) === 1022)
  }

  test("countChange: BSL worked example") {
    assert(countChange(10, List(3, 2, 1)) === 14)
  }

  test("countChange: BSL simple worked example") {
    assert(countChange(7, List(2, 1)) === 4)
  }

  test("countChange: no coins") {
    assert(countChange(10, List()) === 0)
  }

  test("countChange: no money") {
    assert(countChange(0, List(1, 2)) === 0)
  }

  test("countChange: insufficiently low denominations") {
    assert(countChange(10, List(15, 20)) === 0)
  }

}
