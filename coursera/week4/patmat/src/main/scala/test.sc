def addCharToCountList(char: Char, counts: List[(Char, Int)]): List[(Char, Int)] =
    counts match {
      case Nil => List((char, 1))
      case (testChar, testCount) :: pairs =>
        if (char == testChar) (char, testCount + 1) :: pairs
        else (testChar, testCount) :: addCharToCountList(char , pairs)
    }

val A1 = addCharToCountList('A', Nil)
val A1B1 = addCharToCountList('B', A1)
val A2B1 = addCharToCountList('A', A1B1)
addCharToCountList('C', A2B1)