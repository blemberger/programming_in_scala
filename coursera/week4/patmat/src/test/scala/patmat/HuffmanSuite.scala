package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }


  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }


  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times(\"ABA\")") {
    assert(times(List('A', 'B', 'A')) === List(('A', 2), ('B', 1)))
  }


  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("not isSingleton for Nil") {
    assert(!singleton(Nil))
  }

  test("not isSingleton for list size > 1") {
    assert(!singleton(List(Leaf('A', 1), Leaf('B', 1))))
  }

  test("singleton for list size = 1") {
    assert(singleton(List(Leaf('A', 1))))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("combine of Nil") {
    assert(combine(Nil) === Nil)
  }

  test("combine of single Leaf") {
    val leaflist = List(Leaf('A', 10))
    assert(combine(leaflist) === leaflist)
  }

  test("combine of leaf list out of order") {
    val leaflist = List(Leaf('e', 3), Leaf('t', 5), Leaf('x', 1), Leaf('z', 1))
    assert(combine(leaflist) === List(Leaf('x', 1), Leaf('z', 1), Fork(Leaf('e', 3), Leaf('t', 5), List('e', 't'), 8)))
  }

  test("until") {
    val leaflist = List(Leaf('e', 3), Leaf('t', 5), Leaf('x', 1), Leaf('z', 1))
    val left = Fork(Leaf('x', 1), Leaf('z', 1), List('x', 'z'), 2)
    val right = Fork(Leaf('e', 3), Leaf('t', 5), List('e', 't'), 8)
    assert(until(singleton, combine)(leaflist) === List(Fork(left, right, List('x', 'z', 'e', 't'), 10)))
  }

  test("create code tree") {
    assert(createCodeTree(List('x', 'e', 't', 'e', 't', 't', 'z', 't', 'e', 't')) ===
      Fork(Leaf('t', 5), Fork(Fork(Leaf('z', 1), Leaf('x', 1), List('z', 'x'), 2), Leaf('e', 3), List('z', 'x', 'e'), 5), List('t', 'z', 'x', 'e'), 10))
  }

  test("decode using simple tree") {
    val tree = Fork(Leaf('t', 5),
                    Fork(Fork(Leaf('z', 1), Leaf('x', 1), List('z', 'x'),2), Leaf('e', 3), List('z', 'x', 'e'), 5),
                    List('t', 'z', 'x', 'e'), 10)
    val code: List[Bit] = List(0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0)
    assert(decode(tree, code) === List('t', 'e', 'z', 'e', 'x', 'e', 't'))
  }

  test("encode using simple tree") {
    val tree = Fork(Leaf('t', 5),
      Fork(Fork(Leaf('z', 1), Leaf('x', 1), List('z', 'x'),2), Leaf('e', 3), List('z', 'x', 'e'), 5),
      List('t', 'z', 'x', 'e'), 10)
    val string = List('t', 'e', 'z', 'e', 'x', 'e', 't')
    assert(encode(tree)(string) === List(0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0))
  }

  test("decode secret") {
    val secret = decodedSecret.mkString
    println(secret)
    assert(secret === "huffmanestcool")
  }

  test("encode secret") {
    assert(encode(frenchCode)(decodedSecret) === secret)
  }

  test("codebits") {
    val table = List(('t', List(0)),('e', List(1, 1)), ('z', List(1, 0 ,0)), ('x', List(1, 0, 1)))
    assert(codeBits(table)('t') === List(0))
    assert(codeBits(table)('e') === List(1, 1))
    assert(codeBits(table)('z') === List(1, 0 ,0))
    assert(codeBits(table)('x') === List(1, 0, 1))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("quickEncode using simple tree") {
    val tree = Fork(Leaf('t', 5),
      Fork(Fork(Leaf('z', 1), Leaf('x', 1), List('z', 'x'),2), Leaf('e', 3), List('z', 'x', 'e'), 5),
      List('t', 'z', 'x', 'e'), 10)
    val string = List('t', 'e', 'z', 'e', 'x', 'e', 't')
    assert(quickEncode(tree)(string) === List(0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0))
  }

  test("quickEncode secret") {
    assert(quickEncode(frenchCode)(decodedSecret) === secret)
  }

  test("decode and quickEncode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
    }
  }
}
