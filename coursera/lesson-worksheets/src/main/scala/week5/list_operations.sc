def last[T](xs: List[T]): T = xs match {
  case Nil => throw new Error("last of empty list")
  case y :: Nil => y
  case y :: ys => last(ys)
}

last(List(3))
last(List(1, 2, 3))

def init[T](xs: List[T]): List[T] = xs match {
  case List() => throw new Error("init of empty list")
  case List(_) => Nil
  case y :: ys => y :: init(ys)
}

init(List(3))
init(List(1, 2, 3))

def removeAt[T](n: Int, xs: List[T]) = xs.take(n) ++ xs.drop(n + 1)

// My solution (too long):
def removeAt2[T](n: Int, xs: List[T]) = try {
  val Elem = xs(n)

  def removeElemRecurse(xs: List[T], accum: List[T]): List[T] = xs match {
      case Nil => xs
      case Elem :: ys => accum ++ ys
      case y :: ys => removeElemRecurse(ys, y :: accum)
  }
    removeElemRecurse(xs, Nil)
}
catch {
    case e: IndexOutOfBoundsException => xs
}

removeAt(1, Nil)
removeAt(4, Nil)
removeAt(4, List(1, 2, 3))
removeAt(1, List('a', 'b', 'c', 'd')) // List(a, c, d)

removeAt2(1, Nil)
removeAt2(4, Nil)
removeAt2(4, List(1, 2, 3))
removeAt2(1, List('a', 'b', 'c', 'd')) // List(a, c, d)

def flatten(xs: List[Any]): List[Any] = xs match {
  case Nil => xs
  case (y: List[Any]) :: ys => flatten(y) ::: flatten(ys)
  case y :: ys => y :: flatten(ys)
}

val l = List(List(1, 1), 2, List(3, List(5, 8)))
flatten(l)

