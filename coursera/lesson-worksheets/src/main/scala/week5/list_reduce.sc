val l: List[Int] = List(7)

l.reduceLeft(_ * _)

l.reduceRight(_ * _)

List(1, -2, 4).reduceRight(_ * _)

def mapFun[T, U](xs: List[T], f: T => U): List[U] =
  (xs foldRight List[U]())((y: T, ys: List[U]) => f(y) :: ys)

mapFun(List(1, 2, 3, 4), (x: Int) => "x" * x)

def lengthFun[T](xs: List[T]): Int =
  (xs foldRight 0)( (y: T, z: Int) => z + 1 )

lengthFun(List(1, 2, 3, 4))

lengthFun(List(1, 2, 3, 4, 7))

lengthFun(List())