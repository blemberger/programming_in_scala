
// Uses implicit parameter
def scaleList(xs: List[Double])(implicit factor: Double): List[Double] =
  xs match {
    case Nil => xs
    case x :: xs => x * factor :: scaleList(xs) // calls with implicit param "factor"
  }

scaleList(List(1, 2, 3, 4, 5))(3)

def squareList(xs: List[Int]): List[Int] =
  xs match {
    case Nil => xs
    case y :: ys => y * y :: squareList(ys)
  }

def squareList2(xs: List[Int]): List[Int] =
  xs map (x => x * x)

squareList(List(-1, 2, -3, 4, -5))
squareList2(List(-1, 2, -3, 4, -5))

// Quiz 1
def pack[T](xs: List[T]): List[List[T]] = xs match {
  case Nil => Nil
  case x :: xs1 => {
    val (dupes, nondupes) = xs.span(y => x == y)
    dupes :: pack(nondupes)
  }
}

val p = pack(List("a", "a", "a", "b", "c", "c", "a"))
assert(p == List(List("a", "a", "a"), List("b"), List("c", "c"), List("a")))

// Quiz 2 - Run Length Encoding
def encode[T](xs: List[T]): List[(T, Int)] =
  pack(xs).map((x: List[T]) => (x.head, x.size))

val e = encode(List("a", "a", "a", "b", "c", "c", "a"))
assert(e == List(("a", 3), ("b", 1), ("c", 2), ("a", 1)))