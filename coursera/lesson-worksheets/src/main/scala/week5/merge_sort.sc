def merge(xs: List[Int], ys: List[Int]): List[Int] =
  (xs, ys) match {
    case (_, Nil) => xs
    case (Nil, _) => ys
    case (x :: xs1, y :: ys1) =>
      if (x < y) x :: merge(xs1, ys)
      else y :: merge(xs, ys1)
  }

val l1 = List(0, 4, 6, 7, 10)
val l2 = List(1, 3, 8, 9)

merge(l1, l2)
