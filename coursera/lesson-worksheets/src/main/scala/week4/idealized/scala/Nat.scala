package week4.idealized.scala


//Peano numbers
abstract class Nat(val m: Int) {

  def isZero: scala.Boolean
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def + (that: Nat): Nat
  def - (that: Nat): Nat =
    if (that.isZero) this
    else this.predecessor - that.predecessor

  override def toString: String = m.toString
}

object Zero extends Nat(0) {

  def isZero = true

  def predecessor: Nat = throw new Exception("Negative number")

  def + (that: Nat) = that
}

class Succ(n: Nat) extends Nat(n.m + 1) {

  def isZero = false

  def predecessor: Nat = n

  def + (that: Nat) = n + that.successor
}
