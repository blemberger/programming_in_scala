import week4.idealized.scala._

val one = Zero.successor
val two = one.successor
val three = two.successor
val four = three.successor

four.predecessor
three.predecessor
one.predecessor

Zero + one
one + one

val five = four + one

four - four
four - three
five - three
Zero - Zero
three - four
