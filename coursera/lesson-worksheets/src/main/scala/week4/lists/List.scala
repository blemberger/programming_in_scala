package week4.lists

trait List[+T] {
  def head: T
  def tail: List[T]
  def isEmpty: Boolean
  def foreach(f: T => Unit): Unit =
    if (!isEmpty) {
      f(head)
      tail.foreach(f)
    }
  def prepend[U >: T](elem: U): List[U] = new Cons(elem, this)
}

// Can turn into object by type parameterizing with Nothing & making List covariant
object Nil extends List[Nothing] {
  def head = throw new java.util.NoSuchElementException("head of EmptyList")
  def tail = throw new java.util.NoSuchElementException("tail of EmptyList")
  def isEmpty = true
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false
}
