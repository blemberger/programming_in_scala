import week4.expression._

var one: Expr = Number(1)
var three: Expr = Number(3)
var four: Expr = Number(4)
var ten: Expr = Number(10)
var sum: Expr = Sum(one, one)
var prod: Expr = Prod(three, ten)
var v: Expr = Var("bleel")

var complexExpr: Expr = Sum(Prod(Number(2), Var("x")), Var("y"))

three.eval
sum.eval
prod.eval

three.show
sum.show
prod.show
v.show
complexExpr.show()

// Not recommended
v.asInstanceOf[Var].sayHello()