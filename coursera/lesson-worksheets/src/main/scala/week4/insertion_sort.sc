def insert (x: Int, xs: List[Int]): List[Int] = xs match {
    case Nil => List(x)
    case y :: ys => if (x < y) x :: xs
    else y :: insert(x, ys)
}

def isort (xs: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case y :: ys => insert(y, isort(ys))
}

isort(Nil)
isort(List(2))
isort(List(2, 3))
isort(List(3, 2))
isort(List(2, 2))
isort(List(1, 1, 1))

isort(List(10, 8, 3, 2, 1, 0, 11, 5, 7))
isort(List(5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5))