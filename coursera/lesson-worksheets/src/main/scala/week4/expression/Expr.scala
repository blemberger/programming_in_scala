package week4.expression

trait Expr {

  def eval(): Int = this match {
      case Number(n) => n
      case Sum(x, y) => x.eval() + y.eval()
      case Prod(x, y) => x.eval() * y.eval()
  }

  def show(): String = this match {
      case Number(n) => n.toString
      case Sum(x, y) => x.show + " + " + y.show
      case Prod(x, y) => x.show + " * " + y.show
      case Var(name) => name
  }
}

case class Number(n: Int) extends Expr

case class Sum(e1: Expr, e2: Expr) extends Expr

case class Prod(e1: Expr, e2: Expr) extends Expr

case class Var(name: String) extends Expr {
    val x: String = "Hello"

    def sayHello(): String = x
}
