import week4.lists._

object List {

  def apply[T](): List[T] = Nil

  def apply[T](x: T): List[T] = new Cons[T](x, List())

  def apply[T](x: T, y: T): List[T] = new Cons(y, List(x))

}

val empty = List()
assert(empty.isEmpty)

val one = List(1)
assert(!one.isEmpty)
assert(one.head == 1)

val two = List(2, 3)
assert(!two.isEmpty)
assert(two.head == 3)
assert(two.tail.head == 2)