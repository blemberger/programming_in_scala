package week3

class Rational(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private def g = gcd(x, y)

  require(y != 0, "Denominator cannot be zero")

  def this(a: Int) = this(a, 1)

  def numer = x/g
  def denom = y/g

  def add(that: Rational) =
    new Rational(that.numer*denom + numer*that.denom, that.denom*denom)

  def neg() = new Rational(-numer, denom)

  def minus(that: Rational) = add(that.neg)

  def less(that: Rational): Boolean = that.numer * this.denom > this.numer * that.denom

  def max(that: Rational) = if (this.less(that)) that else this

  override def toString() = {

    numer + "/" + denom
  }
}