trait List[T] {
  def isEmpty(): Boolean
  def head(): T
  def tail(): List[T]
}

class Nil[X] extends List[X] {
  def isEmpty = true
  def head() = throw new NoSuchElementException("Nil.head")
  def tail() = throw new NoSuchElementException("Nil.tail")
}

class Cons[S]( val head: S, val tail: List[S]) extends List[S] {
  def isEmpty = false
}

def nth [T] (n: Int, ls: List[T] ): T =
  if (ls.isEmpty()) throw new IndexOutOfBoundsException("index " + n + " is out of bounds")
  else if (n == 0) ls.head
  else nth(n - 1, ls.tail)

val ws = new Cons(-1, new Nil)
val xs = new Cons(3, ws)
val ys = new Cons(6, xs)
val zs = new Cons(7, ys)
nth(3, zs)
nth(2, zs)
nth(1, zs)
nth(0, zs)
//nth(10, zs)
//nth(0, new Nil)
nth(0, ws)
//nth(1, ws)