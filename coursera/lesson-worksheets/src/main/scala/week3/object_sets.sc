abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
  def union(other: IntSet): IntSet
}

class NonEmpty(value: Int, left: IntSet, right: IntSet) extends IntSet {

  def incl(x: Int) = if (x < value) new NonEmpty(value, left incl x, right)
                      else if (x > value) new NonEmpty(value, left, right incl x)
                      else this

  def contains(x: Int) = if (x < value) left contains x
                          else if (x > value) right contains x
                          else true

  def union(other: IntSet) =
      left union (right union (other incl value))

  override def toString: String = "{" + left +  value +  right + "}"
}

object Empty extends IntSet {
  def incl(x: Int) = new NonEmpty(x, Empty, Empty)
  def contains(x: Int) = false
  def union(other: IntSet): IntSet = other

  override def toString: String = "."
}

val t1 = new NonEmpty(3, Empty, Empty)
val t2 = t1 incl 4
val t3 = new NonEmpty(6, Empty, Empty) incl 7
val t4 = t2 union t3
t4 union Empty
