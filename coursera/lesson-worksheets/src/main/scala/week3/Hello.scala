package week3

/**
  * Created by brian with care.
  */
class Hello(val msg: String) {

  def printIt: Unit = {
    println(msg)
  }

}

object Hello {
  def main(args: Array[String]): Unit = {
    new Hello("Hi!!").printIt
    val h = new Hello("Hiya!!")
    h.printIt
    println(h.msg)
  }
}