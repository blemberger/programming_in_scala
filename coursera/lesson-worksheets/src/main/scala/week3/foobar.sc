trait IntSet {
  val myInt: Int

  def incl(x: Int): IntSet
  def contains(x: Int): Boolean = myInt % 2 == 0
  def union(other: IntSet): IntSet
}

abstract class Base(v: Int) {
  def foo = v
  def bar: Int
}

class Sub(v1: Int, v2: Int) extends Base(v1) with IntSet {
  override val myInt = v2
  override def foo = 2
  def bar = 3

  // these methods do not override those in the trait
  def union(o: IntSet) = null
  def incl(g: Int) = null
}

val s = new Sub(1, 4)
s.contains(50) // true because 4 % 2 = 0