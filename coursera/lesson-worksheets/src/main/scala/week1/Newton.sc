/***
 * Redefine abs here even though its in Math package
 * @param x - the square of the number to look for
 * @return abs(x)
 */
def abs(x:Double) = if (x < 0) -x else x

/***
 * From Newton's method looking for an xnext such that:
 * abs( f(xnext) - 0 ) < epsilon, or
 * abs(xnext^2 - square) < epsilon
 *
 * However, due to floating point cut-off error for large values of xnext look for:
 * abs(xnext^2 - square < epsilon * square
 *
 * @param guess
 * @param x - the square of the number to look for
 * @return true if abs(f(guess) - 0) < x * epsilon
 */
def isGoodEnough(guess: Double, x: Double) = abs(guess * guess - x) / x  < 0.0001

/***
 * Return value is given by Newton's method, for finding f(x) = 0, with
 * f(x) = x^2 - square, where square is the number to find sqrt(square).
 * From point-slope formula: 0 = f(xprev) + f'(xprev)(xnext - xprev)
 * xnext = xprev - f(xprev)/f'(xprev)
 * xnext = xprev - (xprev^2 - square) / 2xprev = (xprev + (square/xprev)) / 2
 *
 * @param prev - previous guess
 * @param x - the square of the number to look for
 * @return next guess
 */
def improve(prev: Double, x: Double) =(prev + x / prev) / 2


def sqrtIter(guess: Double, x: Double): Double =
  if (isGoodEnough(guess, x)) guess
  else sqrtIter(improve(guess, x), x)

def sqrt(x: Double) = sqrtIter(1.0, x)

sqrt(1.0E50)
sqrt(2)
sqrt(30)