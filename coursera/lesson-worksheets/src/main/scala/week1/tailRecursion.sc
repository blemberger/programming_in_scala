import scala.annotation.tailrec

def factorial(x: Int): Int =
  if (x == 0) 1 else x * factorial(x - 1)

factorial(5)
factorial(10)

// Implemented factorial using tail-call recursion
def factorial2(n: Int) = {

  @tailrec
  def factorialRecursive(x: Int, partial: Int): Int =
    if (x == n) partial else factorialRecursive(x + 1, partial * (x + 1))

  factorialRecursive(0, 1)
}

factorial2(5)
factorial2(10)
factorial2(0)
factorial2(1)
