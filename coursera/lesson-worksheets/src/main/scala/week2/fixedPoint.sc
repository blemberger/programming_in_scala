import math.abs

def fixedPoint(f: Double => Double): Double = {
  val tolerance = 0.0001
  def isGoodEnough(a: Double, b: Double): Boolean = (abs(b - a) / a) < tolerance
  def recurse(guess: Double): Double = {
    println("guess = " + guess)
    val next = f(guess)
    if (isGoodEnough(guess, next)) {
      println("good enough: " + next)
      next
    }
    else recurse(next)
  }
  recurse(1.0)
}

fixedPoint((x: Double) => 1 + x/2)

def averageDamp(f: Double => Double)(x: Double) = (f(x) + x) / 2

def sqrt(x: Double) = fixedPoint(averageDamp((y: Double) => x/y))

sqrt(2)
