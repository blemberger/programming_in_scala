class Rational(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private def g = gcd(x, y)

  require(y != 0, "Denominator cannot be zero")

  def this(a: Int) = this(a, 1)

  def numer = x/g
  def denom = y/g

  def add(that: Rational) =
    new Rational(that.numer*denom + numer*that.denom, that.denom*denom)

  def neg() = new Rational(-numer, denom)

  def minus(that: Rational) = add(that.neg)

  def less(that: Rational): Boolean = that.numer * this.denom > this.numer * that.denom

  def max(that: Rational) = if (this.less(that)) that else this

  override def toString() = {

    numer + "/" + denom
  }
}

val x3 = new Rational(1, 3)
x3.numer
x3.denom
val y3 = new Rational(5, 7)
x3.add(y3)
val z3 = new Rational(3, 2)
x3.minus(y3).minus(z3)

new Rational(2, 3).less(new Rational(1, 2))
new Rational(1, 2).less(new Rational(2, 3))
new Rational(2, 3).max(new Rational(1, 2))
new Rational(1, 2).max(new Rational(2, 3))

val x4 = new Rational(4, 6)

//val strange = new Rational(1, 0)
//strange.add(strange)

val x5 = new Rational(2)


class Rational1(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  require(y != 0, "Denominator cannot be zero")

  def this(a: Int) = this(a, 1)

//  private def g = gcd(x, y)
  def numer = x
  def denom = y

  def add(that: Rational1) =
    new Rational1(that.numer*denom + numer*that.denom, that.denom*denom)

  def neg() = new Rational1(-numer, denom)

  def minus(that: Rational1) = add(that.neg)

  def less(that: Rational1): Boolean = that.numer * this.denom > this.numer * that.denom

  def max(that: Rational1) = if (this.less(that)) that else this

  override def toString() = {
    val g = gcd(numer, denom)
    numer/g + "/" + denom/g
  }
}

val x = new Rational1(1, 3)
x.numer
x.denom
val y = new Rational1(5, 7)
x.add(y)
val z = new Rational1(3, 2)
x.minus(y).minus(z)

new Rational1(2, 3).less(new Rational1(1, 2))
new Rational1(1, 2).less(new Rational1(2, 3))
new Rational1(2, 3).max(new Rational1(1, 2))
new Rational1(1, 2).max(new Rational1(2, 3))

val x1 = new Rational1(4, 6)

//val strange = new Rational(1, 0)
//strange.add(strange)

val x2 = new Rational1(2)

val xx = new Rational(382387454 * 5, 382387454)
xx.add(xx)

val xx1 = new Rational1(382387454 * 5, 382387454)
xx1.add(xx1)  // incorrect result up due to Int overflow




