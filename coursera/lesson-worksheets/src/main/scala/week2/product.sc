// Part 1 - do product()
def product(f: (Int) => Int)(a: Int, b: Int): Int =
  if (a > b) 1 else f(a) * product(f)(a + 1, b)

product((x: Int) => x)(3, 5)

// Can define a product of squares function either as a def, where the full arg list must be specified
def prodSquares(a: Int, b: Int): Int = product((x: Int) => x * x)(a: Int, b: Int)

// or as a val, were no arg list is specified but a return type of function must be
val prodSquares1: (Int, Int) => Int = product((x: Int) => x * x)
prodSquares(3, 5)
prodSquares1(3, 5)

// Showing how the syntactic sugar above works
def product1(f: (Int) => Int): (Int, Int) => Int = {
  def product1f(a: Int, b: Int): Int =
    if (a > b) 1 else f(a) * product1f(a + 1, b)

  product1f
}

def f = product1((x: Int) => x)
f(3, 5)

product1((x: Int) => x)(3, 5)

// Part 2 - define factorial in terms of product()
def factorial(n: Int) = product((x: Int) => x)(1, n)

factorial(0)
factorial(4)
factorial(5)
factorial(6)

// Part 3 - create a general purpose function for product & sum
def general(oper: (Int, Int) => Int, operUnit: Int)(f: (Int) => Int)(a: Int, b: Int): Int =
  if (a > b) operUnit
  else oper(f(a), general(oper, operUnit)(f)(a + 1, b))

general((x: Int, y: Int) => x * y, 1)((x: Int) => x * x)(3, 5)
general((x: Int, y: Int) => x + y, 0)((x: Int) => x * x)(3, 5)

val product2: (Int, Int) => Int = general((x: Int, y: Int) => x * y, 1)((x: Int) => x * x)
product2(3, 5)

def sum: (Int, Int) => Int = general((x: Int, y: Int) => x + y, 0)((x: Int) => x)
sum(3, 5)
