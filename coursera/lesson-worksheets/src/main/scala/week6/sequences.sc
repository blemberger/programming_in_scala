// All combinations (x,y) where x = {1 .. M} and y = {1 ... N}
def allPairs(M: Int, N: Int) =
  1 to M flatMap ((x:Int) => 1 to N map ((y: Int) => (x, y)))

allPairs(10, 6)
allPairs(5, 5)

def scalarProduct(xs: Vector[Double], ys: Vector[Double]) =
  (xs zip ys).map((xy: (Double, Double)) => xy._1 * xy._2).sum

scalarProduct(Vector(1, -2, 3), Vector(4, 5, 6))

// Using a pattern matching function value
def scalarProduct2(xs: Vector[Double], ys: Vector[Double]) =
  (xs zip ys).map{ case (x, y) => x * y }.sum

scalarProduct2(Vector(1, -2, 3), Vector(4, 5, 6))

// Using a for expression
def scalarProduct3(xs: Vector[Double], ys: Vector[Double]) =
  (for ((x, y) <- xs zip ys) yield (x * y)).sum

scalarProduct3(Vector(1, -2, 3), Vector(4, 5, 6))
