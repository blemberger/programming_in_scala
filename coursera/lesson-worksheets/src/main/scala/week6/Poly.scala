package week6

// My first attempt without looking at MO's solution
class Poly1(val terms: Map[Int, Double]) {
  def +(other: Poly1): Poly1 = {
    val combinedM = (this.terms.toList ::: other.terms.toList).groupBy(_._1)
    val sumTerms = for {
                    (exponent, pairs) <- combinedM
                   } yield
                    (exponent, pairs.reduce((pair1, pair2) => (exponent, pair1._2 + pair2._2) )._2)
    new Poly1(sumTerms)
  }

  override def toString: String = {
    (for {(exponent, coeff) <- terms.toList.sorted.reverse} yield coeff + "x^" + exponent).mkString(" + ")
  }
}

// MO's solution
class Poly2(val terms0: Map[Int, Double]) {

  val terms = terms0 withDefaultValue 0.0

  def this(pairs: (Int, Double)*) = this(pairs.toMap)

  def +(other: Poly2): Poly2 = {
    new Poly2(this.terms ++ (other.terms map adjust))
  }

  def adjust(term: (Int, Double)): (Int, Double) = {
    val (exp, coeff) = term
    exp -> (coeff + terms(exp))
  }

  override def toString: String = {
    (for {(exponent, coeff) <- terms.toList.sorted.reverse} yield coeff + "x^" + exponent).mkString(" + ")
  }
}

// Exercise solution
class Poly3(val terms0: Map[Int, Double]) {

  val terms = terms0 withDefaultValue 0.0

  def this(pairs: (Int, Double)*) = this(pairs.toMap)

  def +(other: Poly3): Poly3 = {
    new Poly3(other.terms.foldLeft(terms)(addTerm))
  }

  def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
    val (exp, coeff) = term
    terms + (exp -> (coeff + terms(exp)))
  }

  override def toString: String = {
    (for {(exponent, coeff) <- terms.toList.sorted.reverse} yield coeff + "x^" + exponent).mkString(" + ")
  }
}