def isSafe(col: Int, queens: List[Int]): Boolean = {
  val row = queens.length
  def areAlreadyPlacedQueensSafe(queens: List[Int]): Boolean = queens match {
    case Nil => true
    case q :: qs => (q != col) &&
      (Math.abs(col - q) != (row - qs.length)) &&
      areAlreadyPlacedQueensSafe(qs)
  }
  areAlreadyPlacedQueensSafe(queens)
}

def queens(n: Int): Set[List[Int]] = {
  def placeQueen(k: Int): Set[List[Int]] = {
    if (k == 0) Set(List())
    else
      for {
        queens <- placeQueen(k - 1)
        col <- 0 until n
        if isSafe(col, queens)
      } yield col :: queens
  }
  placeQueen(n)
}

queens(1)
queens(0)
queens(2)
queens(3)
queens(4)
queens(5)
queens(6)
val sols = queens(8)
sols.size

def show(solution: List[Int]): String = {
  val lines =
    for {
      col <- solution
    } yield Vector.fill(solution.length)("* ").updated(col, "X ").mkString
  "\n" + lines.mkString("\n")
}

sols.take(3) map show mkString "\n"

// Some general stuff with for comprehensions
val v = Vector(Set(List()))
for {
  s <- v
  l <- s
  c <- 1 until 4
} yield c :: l