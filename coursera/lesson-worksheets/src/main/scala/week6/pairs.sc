def isPrime(n: Int): Boolean = (2 until n).forall((d: Int) => n % d != 0)

isPrime(3)
isPrime(4)
isPrime(1)
isPrime(10427291)


// weird fact about forall
Nil.forall(_ => false) // true

val n = 7

// Get all pairs from 1 <= j < i < n where i+j is prime
// 1'st attempt
(1 until n).flatMap(i =>
  (1 until i).map(j => (i, j)))
  .filter(pair => isPrime(pair._1 + pair._2))

// Use a pattern match function value
(1 until n).flatMap(i =>
  (1 until i).map(j => (i, j)))
  .filter{ case (x, y) => isPrime(x + y) }

// Use a for expression
for {
  i <- 1 until n
  j <- 1 until i
  if isPrime(i + j)
} yield (i, j)