import scala.io.Source
import java.io.File

val dict_file = new File(new File(System.getProperty("user.home")),
  "training/learn_scala/programming_in_scala/coursera/lesson-worksheets/dict_words.txt")

val in = Source.fromFile(dict_file)

val words = in.getLines().toList filter(s => s forall ((char) => char.isLetter))

val mnemonics = Map(2 -> List('A', 'B', 'C'), 3 -> List('D', 'E', 'F'),
  4 -> List('G', 'H', 'I'), 5 -> List('J', 'K', 'L'), 6 -> List('M', 'N', 'O'),
  7 -> List('P', 'Q', 'R', 'S'), 8 -> List('T', 'U', 'V'),
  9 -> List('W', 'X', 'Y', 'Z'))

/* Invert the map to get 'a' -> '2', 'k' -> '5', etc. */
// My attempt:
val charCode: Map[Char, Char] = mnemonics.flatMap((pair) => {
  val (digit: Int, chars: List[Char]) = pair
  for {
    char <- chars
  } yield (char, digit.toString.head)
}: List[(Char, Char)])

// MO's solution:
val charCode2: Map[Char, Char] =
  for {
    (digit, chars) <- mnemonics
    char <- chars
  } yield (char, digit.toString.head)

/* Take a word and produce its equivalent code replaced by digits */
def wordCode(word: String): String =
  word.toUpperCase map charCode

/* Extract all codes for each word in dictionary and map that code to its list of words */
val wordsForNum: Map[String, List[String]] =
  words.groupBy(wordCode).withDefaultValue(List())

/* Take a number code and find the set of all lists of words that code to it */
def decode(code: String): Set[List[String]] =
  if (code.isEmpty) Set(List())
  else {
    for {
      index <- 1 to code.length
      word <- wordsForNum(code take index)
      laterWords <- decode(code drop index)
    } yield word ::laterWords
  }.toSet

decode("7225247386")  // Scala is fun

def translate(number: String): Set[String] =
  decode(number) map ((words: List[String]) => words.mkString(" "))

translate("7225247386") // Scala is fun
translate("6303197635")  // MJ's phone number
translate("7087388285")  // My phone number
translate("8477492866")  // House phone number
translate("3835559")  // E&R's phone number
