import week6.{Poly1, Poly2, Poly3}

// How flatMap works
val l = List(1, 2, 3, 4, 5)
l.flatMap((x: Int) => List(x + 1, x + 2))

val p11 = new Poly1(Map(1 -> 2.0, 3 -> 4.0, 5 -> 6.2))
val p12 = new Poly1(Map(0 -> 3.0, 3 -> 7.0))

p11 + p12

val p21 = new Poly2(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
val p22 = new Poly2(0 -> 3.0, 3 -> 7.0)

// Poly2 now contains a total function Map
p22.terms(1) // =0.0

p21 + p22

val p31 = new Poly3(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
val p32 = new Poly3(0 -> 3.0, 3 -> 7.0)

p31 + p32


