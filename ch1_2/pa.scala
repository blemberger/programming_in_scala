// explicitly showing the function literal passed to foreach
args.foreach((arg: String) => println(arg))
// shorter hand, using _ wildcard for the only passed argument (in this case,
//  each element in the list
args.foreach(println(_))
// even shorter-hand, when the function literal is one statement and takes only one argument 
//  the parenthesis and argument are optional
args.foreach(println)
