class ChecksumAccumulator {

  private var sum = 0

  def add(b: Byte) { sum += b }

  def checksum(): Int = ~(sum & 0xFF) + 1 
}

import scala.collection.mutable.Map

object ChecksumAccumulator {

  private val cache = Map[String, Int]()

  def calculate(s: String): Int =
    if (cache.contains(s))
      cache(s)
    else {
      val acc = new ChecksumAccumulator
      for (c <- s)
        acc.add(c.toByte)
      val cs = acc.checksum()
      cache += (s -> cs)
      cs
    }
}

val csa = new ChecksumAccumulator()
csa.add(1)
csa.add(2)
println("checksum of csa: " + csa.checksum)

val msg = "Every dog has his day"
val cs = ChecksumAccumulator.calculate(msg)
println(msg + " checksum: " + cs)
