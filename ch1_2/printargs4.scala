// Most functional style
// formatArgs now a pure function
def formatArgs(as: Array[String]): String = { as.mkString("\n") } 

println(formatArgs(args))
