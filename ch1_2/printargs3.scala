// Even more functional style

def printArgs(as: Array[String]): Unit = {
  as.foreach(println) 
}

printArgs(args)
