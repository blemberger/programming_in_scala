// More functional style

def printArgs(as: Array[String]): Unit = {
  for (arg <-as) 
    println(arg)
}

printArgs(args)
